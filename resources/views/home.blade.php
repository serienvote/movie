@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Hallo: {{ Auth::user()->name }}</h1>
    <table class="table">
    @foreach($tradingobjects as $tradingobject)
        <tr>
            <td>
                <div class="col-xs-2"><img class="img-responsive" src="/storage/{{$tradingobject->image}}">
                </div>
                <div class="col-xs-4">
                    <h4><a href="/trade/{{$tradingobject->id}}">{{$tradingobject->title}}</a></h4>
                    <p>{{$tradingobject->genre}}</p>
                    <h4><small>{{$tradingobject->short_description}}</small></h4>
                </div>
                <div class="col-xs-6">
                    <div class="col-xs-6 text-right">
                        <h6><strong>Preis <span class="text-muted">x</span></strong></h6>
                    </div>


                </div>

            </td>
        </tr>
        @endforeach
    </table>

</div>

@endsection
