<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Movietrading</title>



    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">


</head>
<body>
<div id="app">
    <nav class="navbar navbar-inverse bg-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" >
                    Movietrading
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="/home">topics</a></li>
                    <li><a href="#">meine trades</a></li>
                    <li><a href="/meinKonto">mein Konto</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>




<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>



<script>
    $(document).on('click', '.tradingButton', function() {
        //$('#atit').val($(this).data('document_title'));
        $('.modal-title').text('Make your trade');
        $('#betModal').modal('show');


        var q1, q2, price, bettedSum;

        var position = $(this).val();

        var slider = document.getElementById("bettedSum");



        const b=100;

        function valueToQuant(value, q1, q2) {

            return b * Math.log( Math.exp(value / b + Math.log(Math.exp(q1/b) + Math.exp(q2/b))) - Math.exp(q2/b)) - q1;

        }



        if(position=="long") {
            q1= parseInt($('#cumulated_bought').val());
            q2=parseInt($('#cumulated_sold').val());
        } else {
            q1=parseInt($('#cumulated_sold').val());
            q2=parseInt($('#cumulated_bought').val());
        }

        bettedSum=slider.valueAsNumber;
        document.getElementById("dispBettedSum").innerHTML = bettedSum;
        price= slider.valueAsNumber/valueToQuant(slider.valueAsNumber,q1,q2)
        document.getElementById("dispPrice").innerHTML = price;



        slider.oninput = function() {
            bettedSum=this.value;
            document.getElementById("dispBettedSum").innerHTML= bettedSum;
            price = this.valueAsNumber/valueToQuant(this.valueAsNumber,q1,q2)
            document.getElementById("dispPrice").innerHTML = price;
        }


        $('.modal-footer').on('click', '.confirm', function() {

            var quantityBought, quantitySold;
            if(position == "long") {
                quantityBought=bettedSum/price;
                quantitySold=0;
            } else if(position == "short") {
                quantityBought=0;
                quantitySold=bettedSum/price;
            }

            $.ajax({
                type: 'post',
                url: '/makeBet',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'betted_sum': $('#bettedSum').val(),
                    'traded_price': price,
                    'quantity_bought': quantityBought,
                    'quantity_sold': quantitySold,
                    'tradingobject_id': $('#tradingobject').val(),
                    'trader_id': $('#user').val(),
                    'comment' : $('#comment').val()

                },
                success: function() {
                    $(".tradingBox").replaceWith("<div class='tradingBox'><h1>"+"Thank you for trading"+"</h1></div>")
                }
            });
        });





    });






</script>


</body>
</html>
