@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-xs-12 col-lg-9">


        <h1>{{$tradingobject->title}}</h1>
            <div class="timeleft">
                 Endet in 5 Tagen
            </div>
            <p class="lead">{{$tradingobject->short_description}}</p>
        <div class="tradingBox">
            Chancen stehen bei: 10%
            <p>
                <button type="button" value="long"  class="btn btn-success tradingButton delete-modal btn-yes">
                    <span class="bet-btn-inside">
                      <strong>JA</strong><br/>
                      Es wird ein Erfolg
                    </span>
                </button>
               <button type="button" value="short"  class="btn btn-danger tradingButton btn-no">
                   <span class="bet-btn-inside">
                      <strong>NEIN</strong><br/>
                       Es wird kein Erfolg
                    </span>
               </button>
            </p>
        </div>
        </br>
        </br>
        </br>
        <div><h2>Trailer:</h2></div>
        <div class="embed-responsive embed-responsive-16by9">


        <div class=".video-container">
            <iframe width="560" height="315" src="http://www.youtube.com/embed/mLlSB9Hz8hM" frameborder="0" allowfullscreen></iframe>
        </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-3">
        <img class="img-responsive tradingimage" src="/storage/{{$tradingobject->image}}">

    </div>
    </br>



</div>



<div id="betModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>You have {{Auth::user()->credit}}$</p>
                <p>How much do you want to trade?</p>
                <form class="form-horizontal" role="form">
                    <div class="slidecontainer">
                        <input type="range" min="1" max="{{Auth::user()->credit}}" value="{{Auth::user()->credit}}/2" class="slider" id="bettedSum">
                        <p>Value: <span  id="dispBettedSum"></span></p>
                    </div>

                    <div class="form-group">
                        <input type="hidden" value="{{$tradingobject->id}}" id="tradingobject">
                        <input type="hidden" value="{{Auth::user()->id}}" id="user">
                        <input type="hidden" value="{{$tradingobject->cumulated_bought}}" id="cumulated_bought">
                        <input type="hidden" value="{{$tradingobject->cumulated_sold}}" id="cumulated_sold">

                    </div>
                </form>
                <table>
                    <tbody>
                        <tr>
                            <td class="tradingdata">
                                <p class="scenario if-wrong">Price:<span id="dispPrice"></span></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <textarea class='form-control' rows='6' placeholder='Warum haben Sie so getradet? Bitte geben Sie eine Begründung.' id="comment" style='visibility: visible;'></textarea>

                <div class="modal-footer">
                    <button type="button"  class="btn btn-success confirm" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> trade
                    <button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> schließen
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
