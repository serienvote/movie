@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Hallo: {{ Auth::user()->name }}</h1>
        <div class="col-sm-6 col-lg-5">
            <p>Available to bet:</p>
            <p>{{ Auth::user()->credit }} $</p>
        </div>
    </div>
@endsection
