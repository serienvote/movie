<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tradingobject;

class TradingController extends Controller
{
    public function index($id)
    {
        return view ('trade', ['tradingobject' => Tradingobject::findOrFail($id)]);
    }
}
