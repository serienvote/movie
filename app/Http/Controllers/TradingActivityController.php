<?php

namespace App\Http\Controllers;

use App\User;
use App\TradingActivity;
use App\Tradingobject;
use App\Portfolio;
use Illuminate\Http\Request;


class TradingActivityController extends Controller

{


    public function makeBet(Request $req) {
        $tradingActivity = new TradingActivity();
        $tradingActivity -> traded_price= $req->traded_price;
        $tradingActivity -> tradingobject_id = $req-> tradingobject_id;
        $tradingActivity -> trader_id = $req-> trader_id;
        $tradingActivity -> comment = $req -> comment;
        $tradingActivity -> quantity_bought = $req-> quantity_bought;
        $tradingActivity -> quantity_sold = $req-> quantity_sold;

        $tradingActivity->save();


        $tradingobject = Tradingobject::find($req-> tradingobject_id);
        $tradingobject ->cumulated_bought += $req-> quantity_bought;
        $tradingobject ->cumulated_sold += $req-> quantity_sold;

        $tradingobject->save();

        if( Portfolio::where([['trader_id', '=', '$req-> trader_id'],
                                ['tradingobject_id', '=', '$req-> tradingobject_id'],
                                ])->exists()) {
            $portfolio = Portfolio::where([['trader_id', '=', '$req-> trader_id'],
                ['tradingobject_id', '=', '$req-> tradingobject_id'],
            ])->first();

        } else{
            $portfolio = new Portfolio();
            $portfolio -> trader_id = $req-> trader_id;
            $portfolio -> tradingobject_id = $req-> tradingobject_id;
        }

        $portfolio -> sum_quantity_bought += $req-> quantity_bought;
        $portfolio -> sum_quantity_sold += $req-> quantity_sold;
        $portfolio -> sum_aquisition_costs += $req-> betted_sum;

        $portfolio->save();

        $user = User::find($req->trader_id);
        $user -> credit -= $req->traded_price;
        $user->save();

        return response()->json ($tradingActivity);
    }



}
